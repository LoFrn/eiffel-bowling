# Riferimenti

- [Setup di Eiffel Studio in ambiente Linux](https://www.eiffel.org/doc/eiffelstudio/Linux)
- [Manuale compilatore](https://www.eiffel.org/doc/eiffelstudio/Command%20line)
- [Standard ECMA-367 "Eiffel: Analysis, Design and Programming Language"](http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-367.pdf) ([Differenze fra ECMA-367 e Eiffel Studio](https://www.eiffel.org/doc/eiffelstudio/Differences%20between%20standard%20ECMA-367%20and%20Eiffel%20Software%20implementation))
- [Bigino sintassi](http://eiffel-guide.com/)
- Per chi non usa le macchine del laboratorio può essere utile il *container*
  docker `mmonga/eiffel` (vedi anche
  [qui](https://mameli.docenti.di.unimi.it/svigruppo/wiki/IstruzioniEiffel))

```bash
docker pull mmonga/eiffel
# La versione identica a quella del laboratorio si può ottenere con:
# docker pull mmonga/eiffel:silab
docker run -ti --rm -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix -v $(pwd):/home/eiffel mmonga/eiffel
```

# Esercizio 1

(tempo stimato 45')

Reimplementare in Eiffel il [*kata*
"Bowling"](http://www.butunclebob.com/files/downloads/Bowling%20Game%20Kata.ppt),
partendo dai test di Uncle Bob, per comodità riportato anche qui:

> Il gioco del *bowling* divide la gara di ciascun giocatore in 10 *frame*: in
> ogni frame il giocatore ha 2 possibilità di abbattere i 10 birilli (*pin*). Il
> punteggio ottenuto nel *frame* è il numero di birilli abbattuti, maggiorato di
> un premio per gli *spare* e gli *strike*.

> Uno *spare* si verifica quando vengono abbattuti 10 birilli usando i 2
> tentativi. In questo caso il premio è il numero di birilli abbattuto con il
> tiro (*roll*) seguente (effettuato nel prossimo *frame*).

> Uno *strike* si verifica quando vengono abbattuti 10 birilli al primo
> tentativo: in questo caso il secondo tiro del *frame* non viene effettuato. Il
> premio per lo *strike* è il numero di birilli abbattuto con i 2 tiri seguenti
> (effettuati nel prossimo *frame*).

> Se uno *strike* o uno *spare* si verificano nel decimo *frame*, il giocatore
> ha diritto ai tiri necessari ad acquisire il premio relativo. Il decimo
> *frame* può quindi dare luogo a un massimo di 3 tiri. Il punteggio massimo
> ottenibile è 300 punti.


```java
public class BowlingGameTest extends TestCase {
    private Game g;

    @Before
    protected void setUp() throws Exception {
        g = new Game();
    }

    @Test
    public void testGutterGame() throws Exception {
        rollMany(20, 0);
        assertEquals(0, g.score());
    }

    @Test
    public void testAllOnes() throws Exception {
        rollMany(20,1);
        assertEquals(20, g.score());
    }

    @Test
    public void testOneSpare() throws Exception {
        rollSpare();
        g.roll(3);
        rollMany(17,0);
        assertEquals(16,g.score());
    }

    @Test
    public void testOneStrike() throws Exception {
        rollStrike();
        g.roll(3);
        g.roll(4);
        rollMany(16, 0);
        assertEquals(24, g.score());
    }

    @Test
    public void testPerfectGame() throws Exception {
        rollMany(12,10);
        assertEquals(300, g.score());
    }

    @Test
    public void testLastSpare() throws Exception {
        rollMany(9,10);
        rollSpare();
        g.roll(10);
        assertEquals(275, g.score());
    }


    private void rollSpare() {
        g.rollMany(2, 5);
    }

    private void rollStrike() {
        g.roll(10);
    }

    private void rollMany(int n, int pins) {
        for (int i = 0; i < n; i++)
            g.roll(pins);
    }
}
```


Creare la class `GAME` in un *cluster* `bowling` (Tools -> Add Cluster). Per i
test, usare il *framework* `EQA_TEST_SET` (analogo a `junit`) e creare un
*cluster* di `bowling_test`. Il modo più comodo per aggiungere le librerie
necessarie è usare "Create new test" dal tab "AutoTest".

```eiffel
class
	GAME_TEST_SET

inherit
    
    EQA_TEST_SET

-- ....
```

# Esercizio 2

(tempo stimato 45')

Trasformare i test di Uncle Bob in opportuni contratti per le *feature*  della classe `Game`
